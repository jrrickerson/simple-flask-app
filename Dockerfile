FROM python:3.7-alpine
COPY . /app
RUN pip3 install -r /app/requirements.txt

CMD ["python3", "/app/hello.py"]
